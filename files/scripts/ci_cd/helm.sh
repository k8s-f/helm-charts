#!/usr/bin/env bash

CI_MERGE_REQUEST_PROJECT_PATH=$(curl --header "PRIVATE-TOKEN: $CI_JOB_TOKEN" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}" | jq -r '.path_with_namespace')

for i in "$@"
do
case $i in
    --action=*) # action=package|publish
    HELM_ACTION="${i#*=}"
    shift # past argument=value
    ;; 
    --default)
    DEFAULT=YES
    shift # past argument with no value
    ;;
    *)
      # unknown option
    ;;
esac
done


helm_package () {

mkdir -p .packages
while read chart; do
  echo "[PACKAGING CHART $chart to .packages]"
  helm package "$chart" --destination .packages
done < <(find files/charts/ -mindepth 1 -maxdepth 1 -type d)

}

helm_publish() {
  echo "CI_COMMIT_TAG=$CI_COMMIT_TAG"
  echo "CI_COMMIT_BRANCH=$CI_COMMIT_BRANCH"
  helm repo list | grep ${CI_MERGE_REQUEST_PROJECT_PATH//'/'/-} || helm repo add ${CI_MERGE_REQUEST_PROJECT_PATH//'/'/-} "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/helm/stable"
  helm repo update
  find .packages -mindepth 1 -maxdepth 1 -type f -name '*.tgz' -exec sh -c 'basename "$0"' '{}' \; | while read package; do
    CHART_NAME=$(echo $package | sed -e 's/-[0-9]\.[0-9]\.[0-9]\.tgz$//g')
    CHART_VERSION=$(echo $package | sed -e 's/^[a-zA-Z-].*-//g' | sed -e 's/.tgz$//g')
    CHART_EXISTS=$(helm search repo -l ${CI_MERGE_REQUEST_PROJECT_PATH//'/'/-}/${CHART_NAME}  --version ${CHART_VERSION} | { egrep "$REPO_NAME/$CHART_NAME\s"||true; } | { egrep "$CHART_VERSION\s"||true; } | wc -l)
    if [ $CHART_EXISTS = 0 ]; then
      echo "Load chart ${package}"
      cd .packages
      curl --request POST --user gitlab-ci-token:$CI_JOB_TOKEN --form "chart=@${package}" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/helm/api/stable/charts"
      cd ..
    else
      echo "Chart package $package already exists in Helm repo! Skip!"
    fi
done
}

main(){
  if [[ "${HELM_ACTION}" == "helm_package" ]];then
    helm_package
  fi
  if [[ "${HELM_ACTION}" == "helm_publish" ]];then
    helm_publish
  fi

}
main